msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-07-10 02:36-0500\n"
"PO-Revision-Date: 2018-07-10 12:59+0000\n"
"Last-Translator: Daniel Absmeier <abseabsmeier@gmail.com>\n"
"Language-Team: German <https://translate.lubuntu.me/projects/lubuntu/"
"calamares-settings-lubuntu-desktop/de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.1-dev\n"

#: ../lubuntu-calamares.desktop.in.h:1
msgid "Install Ubuntu Studio 23.04"
msgstr "Ubuntu Studio 23.04 installieren"

#: ../lubuntu-calamares.desktop.in.h:2
msgid "Install Ubuntu Studio"
msgstr "Ubuntu Studio installieren"

#: ../lubuntu-calamares.desktop.in.h:3
msgid "Calamares — System Installer"
msgstr "Calamares — System Installer"
